
import 'package:tcc_unesc/model/dto/sensor_dto.dart';
import 'package:tcc_unesc/model/sensor_ph_model.dart';
import 'package:tcc_unesc/model/sensor_solo_model.dart';
import 'package:tcc_unesc/model/sensor_temperatura_umidade_model.dart';

abstract class SensorPhService {
  SensorPhModel fromDTO(objDto);
  SensorDTOModel toDTO(dtoModel);
}

class SensorPhServiceImpl implements SensorPhService{

  @override
  SensorPhModel fromDTO( objDto) {
    SensorPhModel sensorPhModel = new SensorPhModel();
    sensorPhModel.idSensor           = objDto.idSensor;
    sensorPhModel.percentualVariacao = objDto.percentualVariacao;
    sensorPhModel.valorInformado     = objDto.valorInformado;
    sensorPhModel.acao               = objDto.acao;
    sensorPhModel.estufaModel        = objDto.estufaModel;
    print("fromDTO");
    print(sensorPhModel);
    return sensorPhModel;
  }

  @override
  SensorDTOModel toDTO(dtoModel) {
    SensorDTOModel sensorDTOModel = new SensorDTOModel();
    sensorDTOModel.idSensor           = dtoModel.idSensor;
    sensorDTOModel.percentualVariacao = dtoModel.percentualVariacao;
    sensorDTOModel.valorInformado     = dtoModel.valorInformado;
    sensorDTOModel.acao               = dtoModel.acao;
    sensorDTOModel.estufaModel        = dtoModel.estufaModel;
    print("toDTO em Service");
    print(sensorDTOModel);
    return sensorDTOModel;
  }

}
