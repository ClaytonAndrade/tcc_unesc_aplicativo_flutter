
import 'package:tcc_unesc/model/dto/sensor_dto.dart';
import 'package:tcc_unesc/model/sensor_solo_model.dart';
import 'package:tcc_unesc/model/sensor_temperatura_umidade_model.dart';

abstract class SensorSoloService {
  SensorSoloModel fromDTO(objDto);
  SensorDTOModel toDTO(dtoModel);
}

class SensorSoloServiceImpl implements SensorSoloService{

  @override
  SensorSoloModel fromDTO( objDto) {
    SensorSoloModel sensorSoloModel = new SensorSoloModel();
    sensorSoloModel.idSensor           = objDto.idSensor;
    sensorSoloModel.percentualVariacao = objDto.percentualVariacao;
    sensorSoloModel.valorInformado     = objDto.valorInformado;
    sensorSoloModel.acao               = objDto.acao;
    sensorSoloModel.estufaModel        = objDto.estufaModel;
    print("fromDTO");
    print(sensorSoloModel);
    return sensorSoloModel;
  }

  @override
  SensorDTOModel toDTO(dtoModel) {
    SensorDTOModel sensorDTOModel = new SensorDTOModel();
    sensorDTOModel.idSensor           = dtoModel.idSensor;
    sensorDTOModel.percentualVariacao = dtoModel.percentualVariacao;
    sensorDTOModel.valorInformado     = dtoModel.valorInformado;
    sensorDTOModel.acao               = dtoModel.acao;
    sensorDTOModel.estufaModel        = dtoModel.estufaModel;
    print("toDTO em Service");
    print(sensorDTOModel);
    return sensorDTOModel;
  }

}
