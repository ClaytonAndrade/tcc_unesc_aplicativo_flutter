
import 'package:tcc_unesc/model/dto/sensor_dto.dart';
import 'package:tcc_unesc/model/sensor_temperatura_umidade_model.dart';

abstract class SensorTemperaturaUmidaeService {
  SensorTemperaturaUmidadeModel fromDTO(objDto);
  SensorDTOModel toDTO(dtoModel);
}

class SensorTemperaturaUmidaeServiceImpl implements SensorTemperaturaUmidaeService{

  @override
  SensorTemperaturaUmidadeModel fromDTO( objDto) {
    SensorTemperaturaUmidadeModel sensorTemperaturaUmidadeModel = new SensorTemperaturaUmidadeModel();
    sensorTemperaturaUmidadeModel.idSensor           = objDto.idSensor;
    sensorTemperaturaUmidadeModel.percentualVariacao = objDto.percentualVariacao;
    sensorTemperaturaUmidadeModel.valorInformado     = objDto.valorInformado;
    sensorTemperaturaUmidadeModel.acao               = objDto.acao;
    sensorTemperaturaUmidadeModel.estufaModel        = objDto.estufaModel;
    print("fromDTO");
    print(sensorTemperaturaUmidadeModel);
    return sensorTemperaturaUmidadeModel;
  }

  @override
  SensorDTOModel toDTO(dtoModel) {
    SensorDTOModel sensorDTOModel = new SensorDTOModel();
    sensorDTOModel.idSensor           = dtoModel.idSensor;
    sensorDTOModel.percentualVariacao = dtoModel.percentualVariacao;
    sensorDTOModel.valorInformado     = dtoModel.valorInformado;
    sensorDTOModel.acao               = dtoModel.acao;
    sensorDTOModel.estufaModel        = dtoModel.estufaModel;
    print("toDTO em Service");
    print(sensorDTOModel);
    return sensorDTOModel;
  }

}
