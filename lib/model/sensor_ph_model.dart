import 'estufa_model.dart';

class SensorPhModel {
  int idSensor;
  double valorInformado;
  double percentualVariacao;
  String acao;
  EstufaModel estufaModel;
  String dataHora;

  SensorPhModel({
    this.idSensor,
    this.valorInformado,
    this.percentualVariacao,
    this.acao,
    this.estufaModel,
    this.dataHora
  });

  SensorPhModel.fromJson(Map<String, dynamic> json) {
    idSensor = json['idSensor'];
    valorInformado = json['valorInformado'];
    percentualVariacao = json['percentualVariacao'];
    acao = json['acao'];
    estufaModel = json['estufaModel'] != null
        ? new EstufaModel.fromJson(json['estufaModel'])
        : null;
    dataHora = json['dataHora'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idSensor'] = this.idSensor;
    data['valorInformado'] = this.valorInformado;
    data['percentualVariacao'] = this.percentualVariacao;
    data['acao'] = this.acao;
    if (this.estufaModel != null) {
      data['estufaModel'] = this.estufaModel.toJson();
    }
    data['dataHora'] = this.dataHora;
    return data;
  }
}


