class EstufaModel {
  int idEstufa;
  String descricao;

  EstufaModel({this.idEstufa, this.descricao});

  EstufaModel.fromJson(Map<String, dynamic> json) {
    idEstufa = json['idEstufa'];
    descricao = json['descricao'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idEstufa'] = this.idEstufa;
    data['descricao'] = this.descricao;
    return data;
  }
}
