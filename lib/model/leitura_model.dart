import 'estufa_model.dart';

class LeituraModel {
  int idLeitura;
  double valorObtido;
  String tipoLeitura;
  EstufaModel estufaModel;
  String identificacaoSensorEnum;
  String dataHora;

  LeituraModel(
      {this.idLeitura,
        this.valorObtido,
        this.tipoLeitura,
        this.estufaModel,
        this.identificacaoSensorEnum,
        this.dataHora});

  LeituraModel.fromJson(Map<String, dynamic> json) {
    idLeitura = json['idLeitura'];
    valorObtido = json['valorObtido'];
    tipoLeitura = json['tipoLeitura'];
    estufaModel = json['estufaModel'] != null
        ? new EstufaModel.fromJson(json['estufaModel'])
        : null;
    identificacaoSensorEnum = json['identificacaoSensorEnum'];
    dataHora = json['dataHora'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idLeitura'] = this.idLeitura;
    data['valorObtido'] = this.valorObtido;
    data['tipoLeitura'] = this.tipoLeitura;
    if (this.estufaModel != null) {
      data['estufaModel'] = this.estufaModel.toJson();
    }
    data['identificacaoSensorEnum'] = this.identificacaoSensorEnum;
    data['dataHora'] = this.dataHora;
    return data;
  }
}

