import '../estufa_model.dart';

class SensorDTOModel {
  int idSensor;
  double valorInformado;
  double percentualVariacao;
  String acao;
  EstufaModel estufaModel;

  SensorDTOModel({
    this.idSensor,
    this.valorInformado,
    this.percentualVariacao,
    this.acao,
    this.estufaModel
  });

  SensorDTOModel.fromJson(Map<String, dynamic > json){
    idSensor:           json["idSensor"];
    valorInformado:     json["valorInformado"];
    percentualVariacao: json["percentualVariacao"];
    acao:               json["acao"];
    estufaModel =       json['estufaModel'] != null
        ? new EstufaModel.fromJson(json['estufaModel'])
        : null;
  }

  Map<String, dynamic > toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data["idSensor"]           = this.idSensor;
    data["valorInformado"]     = this.valorInformado;
    data["percentualVariacao"] = this.percentualVariacao;
    data["acao"]               = this.acao;
    if (this.estufaModel != null) {
      data['estufaModel']      = this.estufaModel.toJson();
    }
    return data;
  }
}


