//import 'package:flutter/cupertino.dart';
//import 'package:tcc_unesc/model/dto/sensor_dto.dart';
//
//import 'estufa_model.dart';
//
//class SensorTemperaturaUmidadeModel {
//  int idSensor;
//  double valorInformado;
//  double percentualVariacao;
//  String acao;
//  EstufaModel estufaModel;
//
//  SensorDTOModel sensorDTOModel;
//
//  SensorTemperaturaUmidadeModel({
//    this.idSensor,
//    this.valorInformado,
//    this.percentualVariacao,
//    this.acao,
//    this.estufaModel
//  });
//
//  SensorTemperaturaUmidadeModel.fromJson(Map<String, dynamic> json){
//    idSensor           = json["idSensor"];
//    valorInformado     = json["valorInformado"];
//    percentualVariacao = json["percentualVariacao"];
//    acao               = json["acao"];
//    estufaModel        = json['estufaModel'] != null
//        ? new EstufaModel.fromJson(json['estufaModel'])
//        : null;
//  }
//
//  Map toJson(){
//    final Map data = new Map();
//    data["idSensor"]           = this.idSensor;
//    data["valorInformado"]     = this.valorInformado;
//    data["percentualVariacao"] = this.percentualVariacao;
//    data["acao"]               = this.acao;
//    if (this.estufaModel != null) {
//      data['estufaModel']      = this.estufaModel.toJson();
//    }
//    return data;
//  }
//}
import 'estufa_model.dart';

class SensorTemperaturaUmidadeModel {
  int idSensor;
  double valorInformado;
  double percentualVariacao;
  String acao;
  EstufaModel estufaModel;
  String dataHora;

  SensorTemperaturaUmidadeModel({
    this.idSensor,
    this.valorInformado,
    this.percentualVariacao,
    this.acao,
    this.estufaModel,
  });

  SensorTemperaturaUmidadeModel.fromJson(Map<String, dynamic> json) {
    idSensor = json['idSensor'];
    valorInformado = json['valorInformado'];
    percentualVariacao = json['percentualVariacao'];
    acao = json['acao'];
    estufaModel = json['estufaModel'] != null
        ? new EstufaModel.fromJson(json['estufaModel'])
        : null;
    dataHora = json['dataHora'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idSensor'] = this.idSensor;
    data['valorInformado'] = this.valorInformado;
    data['percentualVariacao'] = this.percentualVariacao;
    data['acao'] = this.acao;
    if (this.estufaModel != null) {
      data['estufaModel'] = this.estufaModel.toJson();
    }
    data['dataHora'] = this.dataHora;
    return data;
  }
}

