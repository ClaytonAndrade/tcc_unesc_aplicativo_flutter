import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tcc_unesc/model/estufa_model.dart';
import 'package:tcc_unesc/model/sensor_temperatura_umidade_model.dart';
import 'package:tcc_unesc/repository/sensor_temperatura_umidade_repository.dart';
import 'package:tcc_unesc/screens/splash_screen.dart';
import 'package:tcc_unesc/utils/unesc_colors.dart';
import 'package:tcc_unesc/utils/unesc_strings.dart';

class SensoreTemperatuara extends StatefulWidget {

  @override
  _SensoreTemperatuaraState createState() => _SensoreTemperatuaraState();
}

class _SensoreTemperatuaraState extends State<SensoreTemperatuara> {
  SensorTemperaturaUmidaeRepositoryImpl sensorRepository;

  String valorInformado = '0,00';
  String percentualVariacao = '0,00';
  double percentual = 0.00;
  String acao = '';
  int    estufa;
  String estufaDescricao;
  EstufaModel estufaModel;
  double valor;

  @override
  void initState() {
    sensorRepository = new SensorTemperaturaUmidaeRepositoryImpl();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(UnescStrings.nameApp,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: FutureBuilder<SensorTemperaturaUmidadeModel>(
            future: sensorRepository.getSensor(1),
            builder: (context, snapshot) {
              print("Snapshot");
              print(snapshot.data);
              print("Snapshot");
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return Center(
                      child: Text("Carregando Dados.....",
                          style: TextStyle(color: Colors.black, fontSize: 25.0),
                          textAlign: TextAlign.center));
                default:
                  if (snapshot.hasError) {
                    return Center(
                        child: Text("Erro ao Carregar Dados....",
                            style:
                            TextStyle(color: Colors.black, fontSize: 25.0),
                            textAlign: TextAlign.center));
                  } else {
                    valorInformado     = snapshot.data.valorInformado.toString();
                    percentual         = snapshot.data.percentualVariacao *100;
                    percentualVariacao = percentual.toString();
                    acao               = snapshot.data.acao.toString();
                    estufa             = snapshot.data.estufaModel.idEstufa;
                    estufaDescricao    = snapshot.data.estufaModel.descricao;
                    estufaModel        = snapshot.data.estufaModel;
                  }
                  return SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          alignment: Alignment.topCenter,
                          child: ImageAsset(),
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(30.0, 15.00, 30.00, 10.00),
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width / 1.2,
                                      child: FlatButton(
                                        color: UnescColors.verdeEscuro,
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(Icons.settings, color: Colors.white),
                                            Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  MediaQuery.of(context).size.width * .03, 0, 0, 0),
                                              child:
                                              Text("Valor Informado: $valorInformado °C",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            )
                                          ],
                                        ),
                                        onPressed: () {
                                          createAlertDialog(context).then((onValue){
                                            print(onValue);
                                            SensorTemperaturaUmidadeModel newObj = new SensorTemperaturaUmidadeModel();
                                            newObj.idSensor = snapshot.data.idSensor;
                                            newObj.valorInformado = double.parse(onValue);
                                            newObj.acao = acao;
                                            newObj.percentualVariacao = percentual /100;
                                            newObj.estufaModel = estufaModel;
                                            sensorRepository.updateSensor(newObj, "t");
                                          });
                                        },
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(30.0)),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width / 1.2,
                                      child: FlatButton(
                                        color: UnescColors.verdeEscuro,
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(Icons.settings, color: Colors.white),
                                            Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  MediaQuery.of(context).size.width * .03, 0, 0, 0),
                                              child: Text("Percentual de variação: $percentualVariacao %",
                                                style: TextStyle(
                                                    color: Colors.white
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        onPressed: () {
                                          createAlertDialog(context).then((onValue){
                                            print(onValue);
                                            this.valor = double.parse(onValue);
                                            if(valor > 100){valor = 100;}
                                            if(valor < 0){valor = 0;}
                                            SensorTemperaturaUmidadeModel newObj = new SensorTemperaturaUmidadeModel();
                                            newObj.idSensor           = snapshot.data.idSensor;
                                            newObj.valorInformado     = snapshot.data.valorInformado;
                                            newObj.acao               = acao;
                                            newObj.percentualVariacao = valor/100;
                                            newObj.estufaModel        = estufaModel;
                                            sensorRepository.updateSensor(newObj, "t");
                                          });
                                        },
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(30.0)),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width / 1.2,
                                      child: FlatButton(
                                        color: UnescColors.verdeEscuro,
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(Icons.info, color: Colors.white),
                                            Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  MediaQuery.of(context).size.width * .03, 0, 0, 0),
                                              child: Text("Ação: $acao",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                        ),
                                        onPressed: () {
                                          Navigator.of(context,);
                                        },
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(30.0)),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width / 1.2,
                                      child: FlatButton(
                                        color: UnescColors.verdeEscuro,
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Icon(Icons.info, color: Colors.white),
                                            Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  MediaQuery.of(context).size.width * .03, 0, 0, 0),
                                              child: Text("Estufa: $estufaDescricao",
                                                style: TextStyle(
                                                    color: Colors.white),
                                              ),
                                            ),
                                          ],
                                        ),
                                        onPressed: () {},
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(30.0)),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
              }
            }
        ),
    );
    }

  Future<String> createAlertDialog(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text("Atualizar dados"),
            content: TextField(keyboardType: TextInputType.number,
              controller: controller,
            ),
            actions: <Widget>[
              MaterialButton(
                elevation: 5.0,
                child: Text("Cancelar"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              MaterialButton(
                elevation: 5.0,
                child: Text("Salvar"),
                onPressed: () {
                  Navigator.of(context).pop(controller.text.toString());
                },
              )
            ],
          );
        });
  }
}
