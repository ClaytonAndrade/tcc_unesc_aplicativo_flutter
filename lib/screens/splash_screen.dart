
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tcc_unesc/utils/my_navigator.dart';
import 'package:tcc_unesc/utils/unesc_colors.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}


class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 5), ()=> MyNavigator.goToHome(context));
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: UnescColors.verdeEscuro,
      body:Center(
        child: ImageAsset(),
      )
    );
  }
}

class ImageAsset extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('assets/images/unesc_logo.png');
    Image image = Image(image: assetImage, width: MediaQuery.of(context).size.width/1.5);
    return Container(child: image,);
  }

}
