
import 'package:flutter/material.dart';
import 'package:tcc_unesc/tabs/controlada_tab.dart';
import 'package:tcc_unesc/tabs/natural_tab.dart';
import 'package:tcc_unesc/tabs/more_tab.dart';
import 'package:tcc_unesc/utils/unesc_colors.dart';
import 'package:tcc_unesc/utils/unesc_strings.dart';

class ControladaScreen extends StatefulWidget {
  @override
  _ControladaScreenState createState() => _ControladaScreenState();
}

class _ControladaScreenState extends State<ControladaScreen> {

  PageController _pageController;
  int _page = 0;

  @override
  void initState(){
    super.initState();

    _pageController = PageController();
  }


  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(UnescStrings.nameApp,
          style: TextStyle(
            color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      bottomNavigationBar: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: UnescColors.verdeEscuro,
          primaryColor: Colors.white,
          textTheme: Theme.of(context).textTheme.copyWith(
            caption: TextStyle(color: Colors.white54)
          )
        ),
        child: BottomNavigationBar(
          currentIndex: _page,
          onTap: (page){
            _pageController.animateToPage(
                page,
                duration: Duration(milliseconds: 500),
                curve: Curves.ease);
          },
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.build),
                title: Text(UnescStrings.controlada)
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.local_florist),
                  title: Text(UnescStrings.natural)
              ),
              BottomNavigationBarItem(
                  icon: Icon(Icons.more_vert),
                  title: Text(UnescStrings.more)
              )
            ]
        ),
      ),
      body: SafeArea(
          child: PageView(
            controller: _pageController,
            onPageChanged: (page){
              setState(() {
                _page = page;
              });
            },
            children: <Widget>[
              ControladaTab(),
              NaturalTab(),
              MoreTab(),
            ],
          ),
      )
    );
  }
}
