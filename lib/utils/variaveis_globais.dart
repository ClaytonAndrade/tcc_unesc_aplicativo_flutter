const SEVER_NAME = "10.0.0.115:8080";
//const SEVER_NAME = "tcc-unesc.herokuapp.com";
const URL_SENSOR_pH = "http://$SEVER_NAME/sensor-ph/";
const URL_SENSOR_SOLO = "http://$SEVER_NAME/sensor-solo/";
const URL_SENSOR_TEMPERATURA_UMIDADE = "http://$SEVER_NAME/sensor-temperatura-umidade/";
const URL_LEITURA_RECENTE_SENSORES_ESTUFA = "http://$SEVER_NAME/leitura/max-sensores-id?idEstufa=";
const Map<String, String> API_HEADERS = {
  'Content-Type': 'application/json; charset=UTF-8'
};
