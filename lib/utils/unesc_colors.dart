import 'dart:ui';

class UnescColors{

  static const Color verdeEscuro   = Color(0xFF62bb50);
  static const Color verdeClaro    = Color(0xFF8fb652);
  static const Color verdeClarinho = Color(0xFFc4d1a2);
}