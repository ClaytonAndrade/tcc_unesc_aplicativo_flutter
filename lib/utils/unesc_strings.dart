class UnescStrings{

  static const String nameApp = "TCC UNESC";
  static const String controlada = "ESTC";
  static const String natural = "ESTN";
  static const String more = "Mais";
  static const String sensor_ph = "Sensor pH";
  static const String sensor_solo = "Sensor solo";
  static const String sensor_umidade = "Sensor umidade";
  static const String sensor_temperatura = "Sensor temperatura";
  static const String urlApi="10.0.0.112:8080/";
}