import 'package:flutter/material.dart';
import 'package:tcc_unesc/screens/sensor_ph.dart';
import 'package:tcc_unesc/screens/sensor_solo.dart';
import 'package:tcc_unesc/screens/sensor_temperatura.dart';
import 'package:tcc_unesc/screens/sensor_umidade.dart';
import 'package:tcc_unesc/utils/unesc_colors.dart';
import 'package:tcc_unesc/utils/unesc_strings.dart';


class MoreTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MoreTabState();
}

class _MoreTabState extends State<MoreTab> {
  @override
  void initState() {
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: UnescColors.verdeClarinho,
        body: Container(
          child: SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                  alignment: Alignment.topCenter,
                  child: ImageAsset(),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(30.0, 180.00, 30.00, 10.00),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: MediaQuery
                                .of(context)
                                .size
                                .width / 1.5,
                            child: FlatButton(
                              color: UnescColors.verdeEscuro,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.settings, color: Colors.white),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(
                                        MediaQuery
                                            .of(context)
                                            .size
                                            .width * .1, 0, 0, 0),
                                    child:
                                    Text(UnescStrings.sensor_temperatura,
                                      style: TextStyle(
                                          color: Colors.white),
                                    ),
                                  )
                                ],
                              ),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => SensoreTemperatuara()
                                ));
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0)),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: MediaQuery
                                .of(context)
                                .size
                                .width / 1.5,
                            child: FlatButton(
                              color: UnescColors.verdeEscuro,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.settings, color: Colors.white),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(
                                        MediaQuery
                                            .of(context)
                                            .size
                                            .width * .1, 0, 0, 0),
                                    child: Text(
                                      UnescStrings.sensor_umidade,
                                      style: TextStyle(
                                          color: Colors.white
                                      ),
                                    ),

                                  ),
                                ],
                              ),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => SensoreUmidade()
                                ));
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0)),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: MediaQuery
                                .of(context)
                                .size
                                .width / 1.5,
                            child: FlatButton(
                              color: UnescColors.verdeEscuro,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.settings, color: Colors.white),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(
                                        MediaQuery
                                            .of(context)
                                            .size
                                            .width * .1, 0, 0, 0),
                                    child: Text(UnescStrings.sensor_solo,
                                      style: TextStyle(
                                          color: Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) => SensorSolo()
                                ));
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0)),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: MediaQuery
                                .of(context)
                                .size
                                .width / 1.5,
                            child: FlatButton(
                              color: UnescColors.verdeEscuro,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Icon(Icons.settings, color: Colors.white),
                                  Container(
                                    padding: EdgeInsets.fromLTRB(
                                        MediaQuery
                                            .of(context)
                                            .size
                                            .width * .1, 0, 0, 0),
                                    child: Text(UnescStrings.sensor_ph,
                                      style: TextStyle(
                                          color: Colors.white),
                                    ),
                                  ),
                                ],
                              ),
                              onPressed: () {Navigator.push(context, MaterialPageRoute(
                                  builder: (context) => SensorPh()
                              ));
                              },
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }

  printIntro(int n) {
    print("Seja bem-vindo");
    print(n);
  }
}

class ImageAsset extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage('assets/images/unesc_logo.png');
    Image image =
        Image(image: assetImage, width: MediaQuery.of(context).size.width / 3);
    return Container(
      child: image,
    );
  }
}
