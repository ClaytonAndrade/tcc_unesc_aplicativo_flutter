import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:tcc_unesc/model/leitura_model.dart';
import 'package:tcc_unesc/repository/leitura_repository.dart';
import 'package:tcc_unesc/utils/unesc_colors.dart';

class ControladaTab extends StatefulWidget {

  @override
  _ControladaTabState createState() => _ControladaTabState();
}

class _ControladaTabState extends State<ControladaTab> {
  LeituraModelImpl leituraModelRepository;

  String _umidade_ar1 = '0,00';
  String _umidade_ar2 = '0,00';
  String _temperatura_ar1 = '0,00';
  String _temperatura_ar2 = '0,00';
  String _umidade_solo = '0,00';
  String _ph_agua = '0,00';

  @override
  void initState() {
    leituraModelRepository = new LeituraModelImpl();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: FutureBuilder<List<LeituraModel>>(
            future: leituraModelRepository.getLeituraData(1),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.none:
                case ConnectionState.waiting:
                  return Center(
                      child: Text("Carregando Dados.....",
                          style: TextStyle(color: Colors.black, fontSize: 25.0),
                          textAlign: TextAlign.center));
                default:
                  if (snapshot.hasError) {
                    return Center(
                        child: Text("Erro ao Carregar Dados....",
                            style:
                                TextStyle(color: Colors.black, fontSize: 25.0),
                            textAlign: TextAlign.center));
                  } else {
                    for (int i = 0; i < snapshot.data.length; i++) {
                      if (snapshot.data[i].identificacaoSensorEnum ==
                          "SENSOR_SOLO_1") {
                        _umidade_solo = snapshot.data[i].valorObtido.toString();
                      } else if (snapshot.data[i].identificacaoSensorEnum ==
                          "SENSOR_TEMPERATURA_1") {
                        _temperatura_ar1 =
                            snapshot.data[i].valorObtido.toString();
                      } else if (snapshot.data[i].identificacaoSensorEnum ==
                          "SENSOR_TEMPERATURA_2") {
                        _temperatura_ar2 =
                            snapshot.data[i].valorObtido.toString();
                      } else if (snapshot.data[i].identificacaoSensorEnum ==
                          "SENSOR_UMIDADE_1") {
                        _umidade_ar1 = snapshot.data[i].valorObtido.toString();
                      } else if (snapshot.data[i].identificacaoSensorEnum ==
                          "SENSOR_UMIDADE_2") {
                        _umidade_ar2 = snapshot.data[i].valorObtido.toString();
                      } else if (snapshot.data[i].identificacaoSensorEnum ==
                          "SENSOR_PH") {
                        _ph_agua = snapshot.data[i].valorObtido.toString();
                      }
                    }
                    return Stack(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * 0.8,
                          color: UnescColors.verdeClarinho,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)),
                                      color: Colors.white,
                                      elevation: 20,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                            title: Text('Umidade Ar 1',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 17.0)),
                                            subtitle: Text(
                                                "Leitura: $_umidade_ar1",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16.0)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)),
                                      color: Colors.white,
                                      elevation: 20,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                            title: Text('Umidade Ar 2',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 17.0)),
                                            subtitle: Text(
                                                'Leitura: $_umidade_ar2',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16.0)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)),
                                      color: Colors.white,
                                      elevation: 20,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                            title: Text('Temperatura Ar 1',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 17.0)),
                                            subtitle: Text(
                                                'Leitura: $_temperatura_ar1',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16.0)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)),
                                      color: Colors.white,
                                      elevation: 20,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                            title: Text('Temperatura Ar 2',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 17.0)),
                                            subtitle: Text(
                                                'Leitura: $_temperatura_ar2',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16.0)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)),
                                      color: Colors.white,
                                      elevation: 20,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                            title: Text('Umidade Solo',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 17.0)),
                                            subtitle: Text(
                                                'Leitura: $_umidade_solo',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16.0)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0)),
                                      color: Colors.white,
                                      elevation: 20,
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                            title: Text('pH Água',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 17.0)),
                                            subtitle: Text('Leitura: $_ph_agua',
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16.0)),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  }
              }
            }
        )
    );
  }
}
