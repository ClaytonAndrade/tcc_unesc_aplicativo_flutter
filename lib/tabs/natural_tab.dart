
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:tcc_unesc/model/leitura_model.dart';
import 'package:tcc_unesc/repository/leitura_repository.dart';
import 'package:tcc_unesc/utils/unesc_colors.dart';


class NaturalTab extends StatefulWidget {
  @override
  _NaturalTabState createState() => _NaturalTabState();
}

class _NaturalTabState extends State<NaturalTab> {
  LeituraModelImpl leituraModelRepository;

  String _umidade_ar = '0,00';
  String _temperatura_ar = '0,00';
  String _umidade_solo = '0,00';

  @override
  void initState(){
    leituraModelRepository = new LeituraModelImpl();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: FutureBuilder<List<LeituraModel>>(
        future: leituraModelRepository.getLeituraData(2),
        builder: (context, snapshot){
          switch(snapshot.connectionState){
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(
                  child: Text("Carregando Dados.....",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 25.0),
                      textAlign: TextAlign.center)
              );
            default:
              if(snapshot.hasError){
                return Center(
                    child: Text("Erro ao Carregar Dados....",
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 25.0),
                        textAlign: TextAlign.center)
                );
              }else{
                for(int i = 0; i < snapshot.data.length ; i++){
                  if(snapshot.data[i].identificacaoSensorEnum == "SENSOR_SOLO_2"){
                    _umidade_solo = snapshot.data[i].valorObtido.toString();
                  }else if (snapshot.data[i].identificacaoSensorEnum == "SENSOR_TEMPERATURA_3"){
                    _temperatura_ar = snapshot.data[i].valorObtido.toString();
                  }else if(snapshot.data[i].identificacaoSensorEnum == "SENSOR_UMIDADE_3"){
                    _umidade_ar = snapshot.data[i].valorObtido.toString();
                  }
                }
                return Stack(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height * 0.8,
                        color: UnescColors.verdeClarinho,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 350,
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                color: Colors.white,
                                elevation: 20,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    ListTile(
                                      title: Text('Sensor Umidade do Ar',
                                          style: TextStyle(
                                              color: Colors.black, fontSize: 20.0)),
                                      subtitle: Text("Leitura: $_umidade_ar",style: TextStyle(
                                          color: Colors.black, fontSize: 20.0)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: 350,
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                color: Colors.white,
                                elevation: 20,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    ListTile(
                                      title: Text('Sensor Temperatura do Ar',
                                          style: TextStyle(
                                              color: Colors.black, fontSize: 20.0)),
                                      subtitle: Text("Leitura: $_temperatura_ar",style: TextStyle(
                                          color: Colors.black, fontSize: 20.0)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              width: 350,
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                color: Colors.white,
                                elevation: 20,
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    ListTile(
                                      title: Text('Sensor Umidade do Solo',
                                          style: TextStyle(
                                              color: Colors.black, fontSize: 20.0)),
                                      subtitle:Text("Leitura: $_umidade_solo",style: TextStyle(
                                          color: Colors.black, fontSize: 20.0)),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
              }
          }
        }
      )
    );
  }
}