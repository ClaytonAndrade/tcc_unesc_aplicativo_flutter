import 'package:flutter/material.dart';
import 'package:tcc_unesc/screens/controlada_sreen.dart';
import 'package:tcc_unesc/screens/splash_screen.dart';
import 'package:tcc_unesc/utils/unesc_colors.dart';

var routes = <String, WidgetBuilder>{
  "/controlada": (BuildContext context) => ControladaScreen()
};

void main() async => runApp(MaterialApp(
    title: 'TCC UNESC',
    theme: ThemeData(
        primaryColor: UnescColors.verdeEscuro,
        accentColor: UnescColors.verdeClaro
    ),
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    routes: routes
));

