import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:tcc_unesc/model/dto/sensor_dto.dart';
import 'package:tcc_unesc/model/sensor_ph_model.dart';
import 'package:tcc_unesc/model/sensor_temperatura_umidade_model.dart';
import 'package:tcc_unesc/repository/sensor_temperatura_umidade_repository.dart';
import 'package:tcc_unesc/service/sensor_temperatura_umidade_service.dart';
import 'package:tcc_unesc/utils/variaveis_globais.dart';

abstract class SensorDtoRepository {
  Future<SensorDTOModel> getSensor(String idEnumSensor);
  Future<SensorDTOModel> updateSensor(SensorDTOModel sensor, int idEnumSensor);

}
class  SensorDtoRepositoryImpl implements SensorDtoRepository {

  SensorTemperaturaUmidaeServiceImpl service = new SensorTemperaturaUmidaeServiceImpl(); 
  
  @override
  Future<SensorDTOModel> getSensor(String idEnumSensor) async {
    if(idEnumSensor == "1"){
      SensorTemperaturaUmidaeRepositoryImpl impl = new SensorTemperaturaUmidaeRepositoryImpl();
      Future<List<SensorTemperaturaUmidadeModel>> obj = impl.getSensor(1) as Future<List<SensorTemperaturaUmidadeModel>>;
      print("SensorDtoRepositoryImpl");
      print("toDTO Repository");
      print(service.toDTO(obj));

      print("É para imprimir service.toDTO(obj)");

      return service.toDTO(obj);
    }
    else if(idEnumSensor == "2"){}
    else if(idEnumSensor == "3"){}
    else if(idEnumSensor == "4"){}
  }

  @override
  Future<SensorDTOModel> updateSensor(SensorDTOModel dto, int idEnumSensor) async {
    if(idEnumSensor == 1){}
    else if(idEnumSensor == 2){}
    else if(idEnumSensor == 3){}
    else if(idEnumSensor == 4){}
    return null;
  }
}