import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tcc_unesc/model/sensor_ph_model.dart';
import 'package:tcc_unesc/utils/variaveis_globais.dart';

abstract class SensorPhRepository {
  Future<SensorPhModel> getSensor(int idEstufa);
  Future<SensorPhModel> updateSensor(SensorPhModel idsensor);

}
class  SensorPhRepositoryImpl implements SensorPhRepository {

  @override
  Future<SensorPhModel> getSensor(int idSensor) async {
    try {
      if (idSensor != null) {
        http.Response response = await http.get('$URL_SENSOR_pH$idSensor');
        print("Resposta Servidor Sensor Temperatura");
        print(jsonDecode(response.body));
        if (response.statusCode == 200) {
          print("if do ststus");
          print(response.statusCode == 200);
          var decode = jsonDecode(response.body);
          print("Decode");
          print(decode);

          print("Retorno");
          return SensorPhModel.fromJson(decode);
         } else {
          throw Exception("Falha ao carregar dados");
        }
      }
    } catch (error) {
      throw Exception('Falha ao carregar dados ' + error);
    }
    return null;
  }

  @override
  Future<SensorPhModel> updateSensor(SensorPhModel idsensor) async {
    try {
      if (idsensor != null) {
        var response = await http.put(
          '$URL_SENSOR_pH${1}',
          headers: API_HEADERS,
          body: jsonEncode(<String, String>{
            'valorInformado':     idsensor.valorInformado.toString(),
            'percentualVariacao': idsensor.percentualVariacao.toString(),
          }),
        );
        if (response.statusCode == 200) {
          return SensorPhModel.fromJson(json.decode(response.body));
        } else {
          throw Exception('Falha ao carregar dados');
        }
      }
    } catch (error) {
      throw Exception('Falha ao atualizar os dados ' + error);
    }
    return null;
  }
}