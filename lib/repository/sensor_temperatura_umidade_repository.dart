import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tcc_unesc/model/sensor_solo_model.dart';
import 'package:tcc_unesc/model/sensor_temperatura_umidade_model.dart';
import 'package:tcc_unesc/utils/variaveis_globais.dart';

abstract class SensorTemperaturaUmidaeRepository {
  Future<SensorTemperaturaUmidadeModel> getSensor(int idEstufa);
  Future<SensorTemperaturaUmidadeModel> updateSensor(SensorTemperaturaUmidadeModel sensor, String tipoLeitura);

}
class  SensorTemperaturaUmidaeRepositoryImpl implements SensorTemperaturaUmidaeRepository {
    @override
    Future<SensorTemperaturaUmidadeModel> getSensor(int idsensor) async {
      try {
        if (idsensor != null) {
          http.Response response = await http.get('$URL_SENSOR_TEMPERATURA_UMIDADE$idsensor');
          print("Resposta Servidor Sensor Temperatura");
          print(jsonDecode(response.body));
          if (response.statusCode == 200) {
            print("if do ststus");
            print(response.statusCode == 200);
            var decode = jsonDecode(response.body);
            print("Decode");
            print(decode);

            print("Retorno");
            print(SensorTemperaturaUmidadeModel.fromJson(decode).percentualVariacao);
            print("O que sai aqui?");

            return SensorTemperaturaUmidadeModel.fromJson(decode);
          } else {
            print("Retornou ERRO do else");
            throw Exception("Falha ao carregar dados");
          }
        }
      } catch (error) {
        print("Retornou ERRO do catch");
        throw Exception('Falha ao carregar dados ' + error);
      }
      return null;
    }

    @override
    Future<SensorTemperaturaUmidadeModel> updateSensor(SensorTemperaturaUmidadeModel sensor, String tipoLeitura) async {
      print("Dados do Update aqui");
      Map<String, String> headers = {"Content-type": "application/json"};
      try {
        print("Entrou try do Update aqui");
        print(jsonEncode(<String, String>{
          'valorInformado':     sensor.valorInformado.toString(),
          'percentualVariacao': sensor.percentualVariacao.toString(),
        }),);

        if (sensor != null) {
          print("If sensor diferente nulo do Update aqui");
          print("$URL_SENSOR_TEMPERATURA_UMIDADE${1}");

          var response = await http.put(
            '$URL_SENSOR_TEMPERATURA_UMIDADE${tipoLeitura == "t" ? 1 : 2}',
            headers: API_HEADERS,
            body: jsonEncode(<String, String>{
              'valorInformado':     sensor.valorInformado.toString(),
              'percentualVariacao': sensor.percentualVariacao.toString(),
            }),
          );

          var response2 = await http.put(
            '$URL_SENSOR_TEMPERATURA_UMIDADE${tipoLeitura == "t" ? 3 : 4}',
            headers: <String, String> {'Content-Type': 'application/json; charset=UTF-8'},
            body: jsonEncode(<String, String>{
              'valorInformado':     sensor.valorInformado.toString(),
              'percentualVariacao': sensor.percentualVariacao.toString(),
            }),
          );

          print(response.body);
          print(response.statusCode);
          print(SensorTemperaturaUmidadeModel.fromJson(json.decode(response.body)));

          if (response.statusCode == 200) {
            return SensorTemperaturaUmidadeModel.fromJson(json.decode(response.body));
          } else {
            throw Exception('Falha ao carregar dados');
          }
        }
        print(sensor);

      } catch (error) {
        throw Exception('Falha ao atualizar os dados ' + error);
      }
      return null;
    }
}