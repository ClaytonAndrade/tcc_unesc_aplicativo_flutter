import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:tcc_unesc/model/leitura_model.dart';
import 'package:tcc_unesc/model/sensor_ph_model.dart';
import 'package:tcc_unesc/model/sensor_solo_model.dart';
import 'package:tcc_unesc/utils/variaveis_globais.dart';

abstract class SensorSoloRepository {
  Future<SensorSoloModel> getSensor(int idEstufa);
  Future<SensorSoloModel> updateSensor(SensorSoloModel sensor);

}
class  SensorSoloRepositoryImpl implements SensorSoloRepository {

    @override
    Future<SensorSoloModel> getSensor(int idsensor) async {
      try {
        if (idsensor != null) {
          http.Response response = await http.get('$URL_SENSOR_SOLO$idsensor');
          print("Resposta Servidor Sensor Temperatura");
          print(jsonDecode(response.body));
          if (response.statusCode == 200) {
            print("if do ststus");
            print(response.statusCode == 200);
            var decode = jsonDecode(response.body);
            print("Decode");
            print(decode);

            print("Retorno");
            return SensorSoloModel.fromJson(decode);
          } else {
            throw Exception("Falha ao carregar dados");
          }
        }
      } catch (error) {
        throw Exception('Falha ao carregar dados ' + error);
      }
      return null;
    }

    @override
    Future<SensorSoloModel> updateSensor(SensorSoloModel sensor) async {
      try {
        if (sensor != null) {
          var response = await http.put(
            '$URL_SENSOR_SOLO${1}',
            headers: API_HEADERS,
            body: jsonEncode(<String, String>{
              'valorInformado':     sensor.valorInformado.toString(),
              'percentualVariacao': sensor.percentualVariacao.toString(),
            }),
          );
          if (response.statusCode == 200) {
            return SensorSoloModel.fromJson(json.decode(response.body));
          } else {
            throw Exception('Falha ao carregar dados');
          }
        }
      } catch (error) {
        throw Exception('Falha ao atualizar os dados ' + error);
      }
      return null;
    }
}