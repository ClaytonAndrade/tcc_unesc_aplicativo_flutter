import 'dart:convert';
import 'package:async/async.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:tcc_unesc/model/leitura_model.dart';
import 'package:tcc_unesc/model/sensor_ph_model.dart';
import 'package:tcc_unesc/utils/variaveis_globais.dart';

abstract class LeituraModelRepository {
  Future<List<LeituraModel>> getLeituraData(int idEstufa);
}

class  LeituraModelImpl implements LeituraModelRepository {
  @override
  Future<List<LeituraModel>> getLeituraData(int idEstufa) async {
    try{
      if (idEstufa != null) {
          http.Response response = await http.get('$URL_LEITURA_RECENTE_SENSORES_ESTUFA$idEstufa');
          print(jsonDecode(response.body));
          if (response.statusCode == 200) {
            var decode = jsonDecode(response.body);
            List<LeituraModel> sm = decode.map<LeituraModel>(
                    (map) {
                  return LeituraModel.fromJson(map);
                }
            ).toList();
            return sm;
          }else{
            throw Exception("Falha ao carregar dados");
          }
      }
    }catch (error) {
      throw Exception('Failed to load SensorPH ' + error);
    }
    return null;
  }
}